# Add authentication

## Add the necessary packages
We'll be using [Guardian](https://github.com/ueberauth/guardian) for authentication, [Argon2](https://github.com/riverrun/argon2_elixir) for hashing users' passwords, and [Comeonin](https://github.com/riverrun/comeonin) as a convinient higher-level API on top of Argon2.
- in `/apps/api_server/mix.esx` add the following packages
```elixir
defp deps do
  [
    # ... some code omitted
    {:guardian, "~> 1.0"},
    {:argon2_elixir, "~> 1.2"}
    {:comeonin, "~> 4.0"},
  ]
end
```
and in the project root, run `mix deps.get` to fetch them.

## Create a Guardian module
Our Guardian module needs to implemet two things:
- `subject_for_token/2` is a method that takes a resource and returns a value for a subject field of token for that resource (we will use ID of our user)
- `resource_from_claims/1` is an opposite of that, it takes the map of claims of a token (subject included), and returns the resource (in our case a user)

Create `apps/api_server/lib/api_server/guardian.ex`:
```elixir
defmodule ApiServer.Guardian do
  use Guardian, otp_app: :api_server

  alias ApiServer.Users

  def subject_for_token(user, _claims) do
    {:ok, user.id}
  end

  def resource_from_claims(%{"sub" => id}) do
    case Users.find(id) do
      {:ok, user} -> {:ok, user}
      {:error, _msg} -> {:error, :resource_not_found}
    end
  end
end
```

Notice we are using `find/1` from ApiServer.Users, which we haven't defined yet. Open `apps/api_server/lib/api_server/users.ex` and add it:
```elixir
defmodule ApiServer.Users do
  import Ecto.Query
  # ... some code omitted
  def find(id) do
    query = from(u in User, where: u.id == ^id)

    query
    |> Repo.one()
    |> case do
      nil -> {:error, "Not found"}
      user -> {:ok, user}
    end
  end
end
```

Next we need to configure Guardian credentials for our app. In `apps/api_server/config/config.exs` add:
```elixir
config :api_server, ApiServer.Guardian,
  issuer: "api_server",
  secret_key: "<your_very_secret_key>"
```
To generate a strong key, guardian even provides a mix task:
```
mix guardian.gen.secret
```

## Add password_hash to users table
To authenticate our users, we need to add a field for `password_hash` into our `users` table, that we will check everytime a user tries to log in.

In `apps/api_server`, run this mix task to create a new migration:
```bash
mix ecto.gen.migration add_password_hash_to_users
```
Open the migration file, and edit it to look like this:
`apps/api_server/priv/repo/migrations/{timestamp}_add_password_hash_to_users`
```elixir
defmodule ApiServer.Repo.Migrations.AddPasswordHashToUsers do
  use Ecto.Migration

  def change do
    alter table(:users) do
      add :password_hash, :string
    end
  end
end
```
Of course we only want to persist the `password_hash` to the database, but we want to accept `password` (and `password_confirmation`) from our users. Let's create virtual attributes for these, in our user model.

`apps/api_server/lib/api_server/user.ex`
```elixir
defmodule ApiServer.User do
  # ... some code ommitted
  schema "users" do
    # ... some code omitted
    field :password_hash, :string
    # Virtual fields:
    field :password, :string, virtual: true
    field :password_confirmation, :string, virtual: true

    timestamps()
  end
  # ... some code omitted
end
```

Next, we will need to update our changeset to validate the presence and "sameness" of password/password_confirmation, and also to actually hash the password before saving to database.

`apps/api_server/lib/api_server/user.ex`
```elixir
defmodule ApiServer.User do
  # ... some code ommitted
  @doc false
  def changeset(user, attrs) do
    user
    |> cast(attrs, [:email, :name, :password, :password_confirmation])
    |> validate_required([:email, :name, :password, :password_confirmation])
    |> unique_constraint(:email)
    |> validate_length(:password, min: 8)
    |> validate_confirmation(:password)
    |> hash_password()
  end

  defp hash_password(changeset) do
    case changeset do
      %Ecto.Changeset{valid?: true, changes: %{password: pass}} ->
        put_change(changeset, :password_hash, Comeonin.Argon2.hashpwsalt(pass))

      _ -> changeset
    end
  end
end
```
Now, let's run the migration. Inside `apps/api_server` run:
```bash
mix ecto.migrate
```
We will also need to update our seed users that we created without passwords. 

First, change the seed script `apps/api_server/priv/repo/seeds.exs`:
```elixir
# ... some code omitted
%User{email: "filip@example.com", name: "Filip Vavera", password: "password", password_confirmation: "password"} |> Repo.insert!
%User{email: "viktor@example.com", name: "Viktor Nawrath", password: "password", password_confirmation: "password"} |> Repo.insert!
```
This will however not change the existing users in our database. We have 2 options:
1) Recreate the database by running following in `apps/api_server`
    ```bash
    mix ecto.drop
    mix ecto.create
    ```
2) Run a quick and dirty update_all command inside elixir's interactive shell. In the root of our ubrella app, run:
    ```bash
    iex -S mix
    # ... some output omitted
    iex(1)> ApiServer.Repo.update_all(ApiServer.User, set: [password_hash: Comeonin.Argon2.hashpwsalt("password")])
    ```
    Disclaimer: This will result in the two users having the same password_hash, and is definitely not a way to handle passwords! It should be OK for a demo app though, and it is a good demonstration of interacting with your phoenix through the elixir shell.

## Add method for authenticating users
Now that we have users with password, we can start writing the logic for authenticating them:
In `apps/api_server/lib/api_server/users.ex`, add an `authenticate/2` function:
```elixir
  def authenticate(email, password) do
    query = from(u in User, where: u.email == ^email)

    case Repo.one(query) do
      nil -> 
        Comeonin.Argon2.dummy_checkpw() # against timing attacks
        {:error, :invalid_credentials}

      user ->
        if Comeonin.Argon2.checkpw(password, user.password_hash) do
          {:ok, user}
        else
          {:error, :invalid_password}
        end
  end
```

## Add login query
Next, we need to add a `login` query and a corresponding resolver to authenticate our users. The query will expect users email and password as arguments, and will return an access token to the user.

Add the login field to our schema `apps/api_server_web/lib/api_server_web/schema.ex`:
```elixir
defmodule ApiServerWeb.Schema do
  # ... some code omitted
  query do
    # ... some code omitted

    @desc "Login with email/password to get access token"
    field :login, :session do
      arg(:email, non_null(:string))
      arg(:password, non_null(:string))

      resolve(&Resolvers.Users.login/3)
    end
  end
end
```
The login field uses a new type (`:session`) and a new resolver. We need to define both.

`apps/api_server_web/lib/api_server_web/schema/user_types.ex`
```elixir
defmodule ApiServerWeb.Schema.UserTypes do
  # ... some code omitted

  object :session do
    @desc "Access token for queries requiring authentication"
    field(:token, :string)
  end
end
```

`apps/api_server_web/lib/api_server_web/resolvers/users.ex`
```elixir
defmodule ApiServerWeb.Resolvers.Users do
  alias ApiServer.Users
  alias ApiServer.Guardian

  def login(_parent, %{email: email, password: password}, _res) do
    with {:ok, user} <- Users.authenticate(email, password),
         {:ok, token, _claims} <- Guardian.encode_and_sign(user) do
      {:ok, %{token: token}}
    else
      _ -> {:error, "Authentication failed"}
    end
  end

  # ... some code omitted
end
```
The resolver first try to authenticate the user based on provide email/password and then generate an access token for them. If any of these steps fails, it will return an authentication error.

You can now start your server again with `mix phx.server` and test the login functionality with this query:
```graphql
query {
  login(email: "viktor@example.com", password: "password") {
    token
  }
}
```
Also try to provide a wrong password to verify you get an error.

## Restricting GraphQL access
Restricting access to GraphQL is not so straightforward as for example REST API, where you would just return a 401 status code and be done. [GraphQL specification](https://facebook.github.io/graphql/October2016/#sec-Response-Format) describes how error handling should be done by populating an `errors` key in the JSON response. Requests to our `/graphql` endpoint should not rely on the transport protocol for error codes, and instead should always return a 200 response.

Instead of returning an error code from the `/graphql` endpoint when user is not authenticated, we will try and get the access token from the request headers, load the associated user record, and put it in our GraphQL context. Every resolver that needs and authenticated user can check for it's presence in the context, and populate the `errors` field if it can't find it.

To do this, we'll create a context [plug](https://hexdocs.pm/plug/readme.html) in `apps/api_server_web/lib/api_server_web/plugs/context.ex`
```elixir
defmodule ApiServerWeb.Plug.Context do
  @behaviour Plug
 
  import Plug.Conn
 
  def init(opts), do: opts
 
  def call(conn, _) do
    case Guardian.Plug.current_resource(conn) do
      nil -> 
        conn

      user ->
        put_private(conn, :absinthe, %{context: %{current_user: user}})
    end
  end
end
```

Now, to use this plug for our GraphQL endpoints, we need to create a `:graphql` pipeline in our router to pipe our GraphQL through. 

`apps/api_server_web/lib/api_server_web/router.ex`:
```elixir
defmodule ApiServerWeb.Router do
  # ... some code omitted

  pipeline :graphql do
    # Use our Guardian module
    plug Guardian.Plug.Pipeline,
      module: ApiServer.Guardian

    # Check authorization header for a Bearer token
    plug Guardian.Plug.VerifyHeader, realm: "Bearer"
    # Try to load current user based on the access token
    plug Guardian.Plug.LoadResource, allow_blank: true
    # Call our custom plug to put the current_user into GraphQL context
    plug ApiServerWeb.Plug.Context
  end

  scope "/api" do
    pipe_through :api

    get "/", ApiServerWeb.Controllers.ApiController, :index
  end

  scope "/api" do
    # Pipe both GraphQL endpoints through both the `:api` and `:graphql` pipelines
    pipe_through [:api, :graphql]

    forward("/graphiql", Absinthe.Plug.GraphiQL, schema: ApiServerWeb.Schema)
    forward("/graphql", Absinthe.Plug, schema: ApiServerWeb.Schema)
  end
end
```

Now the last thing we need to do is to change our users resolver to check for current_user in the context, and only render user if there is one.

`apps/api_server_web/lib/api_server_web/resolvers/users.ex`:
```elixir
defmodule ApiServerWeb.Resolvers.Users do
  # ... some code omitted

  def list_users(_parent, _args, %{context: %{current_user: _current_user}}) do
    {:ok, ApiServer.Users.list_users()}
  end

  def list_users(_parent, _args, _res) do
    {:error, "Not authenticated"}
  end
end
```

To try this out, first send a login query, and copy the provided token. Then, in the GraphiQL interface, add a header with name `Authorization` and a value of `Bearer <your_access_token>`. You should only be able to retrieve the users with this header present.
```graphql
query {
  users {
    name,
    email
  }
}
```